<?php
/*
 Template Name: Home
*/
?>

<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
    $attachment_id = get_field('bg_home');
    $size = "full";
    $imagen = wp_get_attachment_image_src( $attachment_id, $size );
?>


<!-- Home Section -->
    <section id="home" class="home-banner" data-parallax="scroll" data-image-src="<?php echo $imagen[0]; ?>">
        <div class="container">
            <div class="welcome-text">
            <?php if( get_field('linea_1') ): ?>
                <h3><?php the_field('linea_1'); ?></h3>
            <?php endif; ?>
            <?php if( get_field('linea_2') ): ?>
                <h2><?php the_field('linea_2'); ?></h2>
            <?php endif; ?>
                <div id="typed-strings">

            <?php if( have_rows('linea_3') ): ?>

            <?php

                while ( have_rows('linea_3') ) : the_row();

                $texto = get_sub_field('texto');

                ?>

                <h3 aria-hidden="true"><?php echo $texto; ?></h3>

            <?php endwhile; ?>

            <?php endif; ?>

                </div>
                <h3 class="typed-text"><span id="typed"></span></h3>
                <a href="#about" class="icon-btn">
                    <i class="fa fa-angle-double-down"></i>
                </a>

            </div>
        </div>
    </section>
    <!-- Home Section end -->


    <!--  About Section -->
    <section id="about" class="section grey-bg">
        <!-- Container -->
        <div class="container">
            <div class="section-title left">
                <h2>Mi Historia</h2>
                <?php if( get_field('mi_subtitulo') ): ?>
                    <p><?php the_field('mi_subtitulo'); ?></p>
                <?php endif; ?>

            </div><!-- .section-title -->

            <div class="row">
                <div class="col-xs-12 col-sm-5">
                    <div class="pers-picture">
                    <?php
                        $attachment_id = get_field('mi_imagen');
                        $size = "large";
                        $imagen2 = wp_get_attachment_image_src( $attachment_id, $size );
                    ?>
                        <img src="<?php echo $imagen2[0]; ?>" alt="Sonia Castro" />
                    </div><!-- .personal-pic -->
                    <br>
                </div><!-- .col-xs-12 col-sm-5 -->

                <div class="col-xs-12 col-sm-7">
                    <div class="pers-info">
                    <?php if( get_field('mi_texto') ): ?>
                    <?php the_field('mi_texto'); ?>
                    <?php endif; ?>
                    </div><!-- .personal-info -->
                </div><!-- .col-xs-12 col-sm-5 -->

            </div><!-- .Row -->
        </div><!-- .Container -->

    </section>
    <!-- About Section end -->


    <!-- books Section-->
    <section id="books" class="section">
        <div class="container">
            <div class="section-title">
                <h2>Libros</h2>
                <?php if( get_field('lb_subtitulo') ): ?>
                    <p><?php the_field('lb_subtitulo'); ?></p>
                <?php endif; ?>
            </div><!-- .section-title -->

            <div class="row books-wrapper">

              <?php if( have_rows('lb_item') ): ?>

                <?php
                while ( have_rows('lb_item') ) : the_row();
                  $attachment_id = get_sub_field('imagen');
                  $size = "large";
                  $image_book = wp_get_attachment_image_src( $attachment_id, $size );
                  $titulo = get_sub_field('titulo');
                  $link_book = get_sub_field('link_book');
                  $link_ebook = get_sub_field('link_ebook');
                ?>

                  <div class="col-xs-6 col-sm-4 col-md-3">

                    <div class="ebook text-center">
                      <div class="overlay">
                        <div class="action">
                          <?php if ( $link_book ) { ?>
                            <a href="<?php echo $link_book; ?>" class="m-btn" title="<?php echo $titulo; ?>" alt="<?php echo $titulo; ?>" target="_blank">Comprar Libro</a>
                          <?php } ?>
                          <?php if ( $link_ebook ) { ?>
                            <a href="<?php echo $link_ebook; ?>" class="m-btn" title="<?php echo $titulo; ?>" alt="<?php echo $titulo; ?>" target="_blank">Comprar eBook</a>
                          <?php } ?>
                        </div>
                      </div>
                      <img src="<?php echo $image_book[0]; ?>" class="img-fluid hvr-grow">
                    </div>

                  </div>

                <?php endwhile; ?>

              <?php endif; ?>

            </div><!-- .row -->
        </div><!-- .container -->
    </section>
    <!-- books Section end -->


    <!-- Services Section-->
    <section id="services" class="section grey-bg">
        <div class="container">
            <div class="section-title">
                <h2>Conferencias</h2>
                <?php if( get_field('cf_subtitulo') ): ?>
                    <p><?php the_field('cf_subtitulo'); ?></p>
                <?php endif; ?>
            </div><!-- .section-title -->

            <div class="row d-table">

                <?php if( have_rows('cf_item') ): ?>

                    <?php

                    while ( have_rows('cf_item') ) : the_row();

                    $icono = get_sub_field('icono');
                    $titulo = get_sub_field('titulo');
                    $duracion = get_sub_field('duracion');
                    $texto = get_sub_field('texto');

                    ?>

                    <div class="col-sm-4 ebook mb-20">
                        <div class="service">
                            <span class="service-icon">
                            <?php if ( $icono ) { ?>
                            <i class="fa <?php echo $icono; ?> fa-2x" aria-hidden="true"></i>
                            <?php } ?>
                            </span>
                            <?php if ( $titulo ) { ?>
                            <h4><?php echo $titulo; ?></h4>
                            <?php } ?>
                            <div class="service-desc">
                            <?php if ( $duracion ) { ?>
                            <p> <?php echo $duracion; ?> </p>
                            <?php } ?>
                            <?php if ( $texto ) { ?>
                            <p><?php echo $texto; ?></p>
                            <?php } ?>
                            </div><!-- .service-desc -->
                        </div><!-- .service-box -->
                    </div><!-- .col-sm-4 -->

                    <?php endwhile; ?>

                <?php endif; ?>

            </div><!-- .row -->
        </div><!-- .container -->
    </section>
    <!-- Services Section end -->



    <div class="portfolio-filter"> </div>

    <!-- clients -->
    <section id="clients" class="section">
        <div class="container">
            <div class="section-title">
                <h2>Clientes</h2>
                <?php if( get_field('cl_subtitulo') ): ?>
                    <p><?php the_field('cl_subtitulo'); ?></p>
                <?php endif; ?>
            </div><!-- .row -->
            <div class="row">
                <div class="col-12">
                    <ul class="clients clients- slider">
                    <?php if( have_rows('cl_item') ): ?>
                    <?php
                    while ( have_rows('cl_item') ) : the_row();
                        $attachment_id = get_sub_field('imagen');
                        $size = "small";
                        $image_client = wp_get_attachment_image_src( $attachment_id, $size );
                        $link_cliente = get_sub_field('enlace');
                    ?>

                    <li>
                    <?php if ( $link_cliente ) { ?>
                        <a href="<?php echo $link_cliente; ?>" target="_blank">
                    <?php } ?>
                        <img src="<?php echo $image_client[0]; ?>" class="img-fluid" width="180" height="100">
                    <?php if ( $link_cliente ) { ?>
                        </a>
                    <?php } ?>
                    </li>
                    <?php endwhile; ?>
                <?php endif; ?>
                    </ul>
                </div>
            </div><!-- .section-title -->
        </div>
        <hr class="clients-sep">
    </section>
    <!-- Services Section end -->

    <!-- Blog Section -->
    <section id="blog" class="section">
        <div class="container">
            <div class="section-title">
                <h2>Prensa</h2>
                <?php if( get_field('pr_subtitulo') ): ?>
                    <p><?php the_field('pr_subtitulo'); ?></p>
                <?php endif; ?>
            </div><!-- .section-title -->
            <div class="row">

            <?php if( have_rows('pr_item') ): ?>

            <?php

            while ( have_rows('pr_item') ) : the_row();

            $attachment_id2 = get_sub_field('imagen');
            $size2 = "medium";
            $image = wp_get_attachment_image_src( $attachment_id2, $size2 );
            $titulo = get_sub_field('titulo');
            $descripcion = get_sub_field('descripcion');
            $enlace = get_sub_field('enlace');


            ?>

                <div class="col-sm-4 col-sm-12">
                    <div class="blog-item">
                        <div class="blog-img">
                            <img src="<?php echo $image[0]; ?>" title="<?php echo $titulo; ?>" alt="<?php echo $titulo; ?>" />
                        </div><!-- .blog-img -->
                        <div class="blog-contents">
                            <h3><?php echo $titulo; ?></h3>
                            <?php if ( $descripcion ) { ?>
                            <div class="blog-meta">
                                <p class="small"><?php echo $descripcion; ?></p>
                            </div>
                            <?php } ?>
                            <?php if ( $enlace ) { ?>
                            <div class="blog-action">
                                <a href="<?php echo $enlace; ?>" class="m-btn" target="_blank">Ver Más</a>
                            </div>
                            <?php } ?>
                        </div><!-- .blog-contents -->
                    </div><!-- .blog-item -->
                </div><!-- .col-sm-4 col-sm-12 -->


                <?php endwhile; ?>

            <?php endif; ?>

            </div><!-- .row -->
        </div><!-- .container -->
    </section>
    <!-- Blog Section end -->



    <!-- Contact Section -->
    <section id="contact" class="section grey-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-title">
                        <h2>Contacto</h2>
                    </div>
                </div>
            </div><!-- .row -->

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <!-- Contact form starts -->
                    <div class="contact-form">

                    <?php echo do_shortcode("[contact-form-7 id='4' title='contacto']"); ?>

                    </div>
                </div><!-- .col-md-8 -->

            </div><!-- .row -->

        </div><!-- .container -->
    </section>
    <!-- Contact Section end -->

    <?php endwhile; else : ?>

            <article id="post-not-found" class="hentry cf">
                    <header class="article-header">
                        <h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
                </header>
                    <section class="entry-content">
                        <p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
                </section>
                <footer class="article-footer">
                        <p><?php _e( 'This is the error message in the page-custom.php template.', 'bonestheme' ); ?></p>
                </footer>
            </article>

    <?php endif; ?>



<?php get_footer(); ?>
