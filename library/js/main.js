/* eslint-disable */
(function($) {
"use strict";


/*=====================================
  Start Loading
  =====================================*/
$(window).on('load', function () {
    $('#loading').fadeOut(1000);
});
/*=====================================
  End Loading
  =====================================*/


/* =====================================
  Header Fixed
  =====================================*/
$(window).on('scroll', function(){
  if ($(window).scrollTop() >= 100) {
    $('header').addClass('fixed-header');
  }
  else {
    $('header').removeClass('fixed-header');
  }
});
/* =====================================
  Header End
  =====================================*/


/*=====================================
  Start Typed
  =====================================*/
$(function () {
  $("#typed").typed({
    stringsElement: $('#typed-strings'),
    typeSpeed: 0,
    startDelay: 0,
    backSpeed: 0,
    backDelay: 3000,
    loop: true,
    loopCount: false,
    showCursor: true,
    cursorChar: "*",
    attr: null,
    contentType: 'html'
  });
});
/*=====================================
  End Typed
  =====================================*/



/*=====================================
  Start Portfolio filter
  =====================================*/
var containerEl = document.querySelector('.portfolio-filter');
var mixer = mixitup(containerEl);
/*=====================================
  End Portfolio filter
  =====================================*/



$(document).ready(function(){
/* =====================================
  Home Banner
  =====================================*/
$(".home-banner").height($(window).height());

$(window).on('resize', function(){
    $(".home-banner").height($(window).height());
});
/* =====================================
  Home Banner end
  =====================================*/



/* =====================================
  Header Scroll
  =====================================*/
$('.header .navbar').onePageNav({
    currentClass: 'current',
    changeHash: false,
    scrollSpeed: 1000
});
/* =====================================
  Header Scroll end
  =====================================*/



/* =====================================
  Banner Icon Scroll
  =====================================*/
$(".icon-btn").on('click', function() {
    $('html,body').animate({
    scrollTop: $("#about").offset().top},
    'slow');
});
/* =====================================
  Banner Icon Scroll end
  =====================================*/



/* =====================================
    Elements animating while entering the viewport
    =====================================*/
$('.animate').viewportChecker({
    classToAdd: 'animated fadeIn',
    offset: 100
});

/* =====================================
    Slider carousel
    =====================================*/

    $('.clients-slider').slick({
    dots: true,
    infinite: true,
    autoplay: true,
    speed: 300,
    arrows: false,
    slidesToShow: 5,
    slidesToScroll: 5,
    responsive: [
    {
    breakpoint: 1140,
    settings: {
    slidesToShow: 4,
    slidesToScroll: 4,
    dots: true
    }
    },
    {
    breakpoint: 600,
    settings: {
    slidesToShow: 2,
    slidesToScroll: 2
    }
    },
    {
    breakpoint: 480,
    settings: {
    dots: false,
    slidesToShow: 1,
    slidesToScroll: 1
    }
  }
    ]
  });
});

})(jQuery);