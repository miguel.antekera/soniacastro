/* eslint-disable */
!function(e){"use strict";
/*=====================================
  Start Loading
  =====================================*/e(window).on("load",function(){e("#loading").fadeOut(1e3)}),
/*=====================================
  End Loading
  =====================================*/
/* =====================================
  Header Fixed
  =====================================*/
e(window).on("scroll",function(){100<=e(window).scrollTop()?e("header").addClass("fixed-header"):e("header").removeClass("fixed-header")}),
/* =====================================
  Header End
  =====================================*/
/*=====================================
  Start Typed
  =====================================*/
e(function(){e("#typed").typed({stringsElement:e("#typed-strings"),typeSpeed:0,startDelay:0,backSpeed:0,backDelay:3e3,loop:!0,loopCount:!1,showCursor:!0,cursorChar:"*",attr:null,contentType:"html"})});
/*=====================================
  End Typed
  =====================================*/
/*=====================================
  Start Portfolio filter
  =====================================*/
var o=document.querySelector(".portfolio-filter"),t=mixitup(o);
/*=====================================
  End Portfolio filter
  =====================================*/
e(document).ready(function(){
/* =====================================
  Home Banner
  =====================================*/
e(".home-banner").height(e(window).height()),e(window).on("resize",function(){e(".home-banner").height(e(window).height())}),
/* =====================================
  Home Banner end
  =====================================*/
/* =====================================
  Header Scroll
  =====================================*/
e(".header .navbar").onePageNav({currentClass:"current",changeHash:!1,scrollSpeed:1e3}),
/* =====================================
  Header Scroll end
  =====================================*/
/* =====================================
  Banner Icon Scroll
  =====================================*/
e(".icon-btn").on("click",function(){e("html,body").animate({scrollTop:e("#about").offset().top},"slow")}),
/* =====================================
  Banner Icon Scroll end
  =====================================*/
/* =====================================
    Elements animating while entering the viewport
    =====================================*/
e(".animate").viewportChecker({classToAdd:"animated fadeIn",offset:100}),
/* =====================================
    Slider carousel
    =====================================*/
e(".clients-slider").slick({dots:!0,infinite:!0,autoplay:!0,speed:300,arrows:!1,slidesToShow:5,slidesToScroll:5,responsive:[{breakpoint:1140,settings:{slidesToShow:4,slidesToScroll:4,dots:!0}},{breakpoint:600,settings:{slidesToShow:2,slidesToScroll:2}},{breakpoint:480,settings:{dots:!1,slidesToShow:1,slidesToScroll:1}}]})})}(jQuery);