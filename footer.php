		<!-- Footer Section -->
    <footer class="footer-section">
        <div class="container">


        <?php if( have_rows('ft_social') ): ?>

        <div class="social-link">
            <ul>

            <?php  

            while ( have_rows('ft_social') ) : the_row();

            $icono = get_sub_field('icono');
            $enlace = get_sub_field('titulo');
            
            ?>

            <?php if( $enlace ): ?>
            <li><a href="<?php echo $enlace; ?>" target="_blank"><i class="fa <?php echo $icono; ?>" aria-hidden="true"></i></a></li>
            <?php endif; ?>

            <?php endwhile; ?> 

            </ul>
        </div><!-- .social-link -->

        <?php endif; ?> 
 
            <div class="address">
                <?php if( get_field('ft_telefono') ): ?>
                    <div class="address-box clearfix">
                        <p> <i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:<?php echo str_replace([':', '(', ')', ' '], '', get_field('ft_telefono')); ?>"><?php the_field('ft_telefono'); ?></a></p>
                    </div>
                <?php endif; ?>
                <?php if( get_field('ft_email') ): ?>
                    <div class="address-box clearfix">
                        <p> <i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:<?php the_field('ft_email'); ?>"><?php the_field('ft_email'); ?></a></p>
                    </div>
                <?php endif; ?>
                
            </div><!-- .address -->

        </div><!-- .container -->
        <p class="copyright">&copy; <?php echo date("Y"); ?> Derechos Reservados.</p>
    </footer><!-- .footer-area -->
    <!-- Footer Section end -->

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! -->
